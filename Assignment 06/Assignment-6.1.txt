#include<stdio.h>

int pattern(int);
int line(int);

int line(int a)
{
	while(a>0)
	{
		printf("%d ",a);
		a=a-1;
	}
	printf("\n");
}

int pattern(int a)
{
	if(a>0)
	{
		pattern(a-1);
	}
	line(a);
}

int main()
{
	int val;
	printf("Enter how many lines required: ");
	scanf("%d",&val);
	pattern(val);
	return 0;
}