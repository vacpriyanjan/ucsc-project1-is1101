#include<stdio.h>

void fibonacciSeq(int x);
int fibonacciValue(int x);

int fibonacciValue(int x)
{
	if(x==0) return 0;
	else if(x==1) return 1;
	else return fibonacciValue(x-1)+fibonacciValue(x-2);
}

void fibonacciSeq(int x)
{
	if(x>=0)
	{
	fibonacciSeq(x-1);
	printf("%d\n",fibonacciValue(x));	
	}
}

int main()
{
	int n;
	printf("Enter a number: ");
	scanf("%d",&n);
	fibonacciSeq(n);
	return 0;
}